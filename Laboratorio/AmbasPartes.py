# Tarea 1
from threading import Thread, Semaphore, Event, Lock, current_thread
import time
import random

class Barberia:
    def __init__(self):
        self.clientesEsperando = [] # lista de clientes en sala
        self.clientesEnSala = Semaphore(3) # controla asientos disponibles en sala de espera
        self.barberoOcupado = Event() # evento que permite dormir, despertar y trabajar
        self.sillon=Semaphore(1) # controla clientes en asiento
        self.mutex = Lock() # Lock de exclusion mutua

    def actividadBarbero(self):
        while True:
            self.mutex.acquire()
            if self.clientesEnSala._value <3:
                cliente = self.clientesEsperando.pop(0)
                self.mutex.release()
                print("El barbero da paso a un cliente")
                self.sillon.acquire() # el sillón del barbero se ocupa
                self.clientesEnSala.release() # se libera un campo en la sala de espera
                self.cortarPelo(cliente) # el barbero corta al cliente
            else:
                self.mutex.release()
                print('¡Ah, todo hecho, a dormir!')
                self.dormir() # el barbero se duerme
                print('El barbero se despertó')

    def actividadCliente(self):
        print("El cliente "+current_thread().name+" llega a la puerta de la barbería")
        
        self.mutex.acquire() #el cliente llega
        if self.clientesEnSala._value > 0: # si hay espacio en la sala el cliente entra
            print("El cliente "+current_thread().name+" entra en la sala de espera")
            self.clientesEnSala.acquire() # se llena un espacio en la sala
            self.clientesEsperando.append(current_thread().name) # se agrega a la lista de clientes esperando
            self.mutex.release() # se libera la exclusión mutua
            self.despertar() # se despierta al barbero si está dormido
        else:
            print("La sala de espera está llena, "+current_thread().name+" se va.")
            self.mutex.release()
            time.sleep(random.randint(1, 5)) # después de un tiempo aleatorio se llama al cliente de nuevo
            self.actividadCliente()

    def dormir(self):
        self.barberoOcupado.wait() # espera a ser despertado

    def despertar(self):
        self.barberoOcupado.set() # despierta al barbero

    def cortarPelo(self,cliente):
        self.barberoOcupado.clear() # el barbero está trabajando
        print("El barbero corta a "+cliente)
        time.sleep(5)
        print("El barbero a terminado de cortar a "+cliente)
        self.sillon.release() # el sillón se libera


if __name__ == '__main__':
    clientes = ['Carlos', 'Juana', 'Marcos', 'Martín', 'Andrea', 'Pedro'] # nombres de los hilos clientes

    barberia = Barberia()
    print('La barbería abre sus puertas')
    barbero_thread = Thread(target=barberia.actividadBarbero)
    barbero_thread.start()

    hilos_clientes = []
    for cliente in clientes:
        hilo_cliente = Thread(target=barberia.actividadCliente, name=cliente)
        hilos_clientes.append(hilo_cliente)
        hilo_cliente.start()
        time.sleep(random.randint(1,2))

    for hilo_cliente in hilos_clientes:
        hilo_cliente.join()
# Tarea 2
import pandas as pd
import multiprocessing as mp
import numpy as np
import time
#modulo para usar expresiones regulares
import re
from multiprocessing import Pool
#importación del corpus brown
import nltk
nltk.download('brown')
from nltk.corpus import brown

# Función que construye el DataFrame para su procesamiento, con una frase por fila
def construye_textos():
    return [" ".join(np.random.permutation(sents)) for sents in brown.sents()]

# Función que reemplaza comillas dobles
def reemplazar_comillas(texto):
    return texto.apply(lambda text: text.replace("``", '"'))

# Función que convierte todas las palabras a minúsculas
def a_minusculas(texto):
    return texto.apply(lambda text: text.lower())

# Función que cuenta palabras de cada fila del dataframe
def contar_palabras(texto):
    return texto.apply(lambda text: len(re.split(r"(?:\s+)|(?:,)|(?:\-)", text)))

# Función que se aplica
def procesar_df(df):
    # Se hace copia del dataframe para no modificarlo
    salida_df = df.copy()

    # Reemplaza las comillas
    salida_df['text'] = reemplazar_comillas(salida_df['text'])

    # Pasa el texto a minúsculas
    salida_df['text'] = a_minusculas(salida_df['text'])

    # Cuenta el número de palabras y construye columna nueva con nombre num_palabras
    salida_df['num_palabras'] = contar_palabras(salida_df['text'])

    # Elimina los textos demasiado largos
    texto_largo_para_eliminar = salida_df[salida_df['num_palabras'] > 50]
    salida_df.drop(texto_largo_para_eliminar.index, inplace=True)

    # Elimina los textos demasiado cortos
    texto_corto_para_eliminar = salida_df[salida_df['num_palabras'] < 10]
    salida_df.drop(texto_corto_para_eliminar.index, inplace=True)

    # Reinicializa los índices
    salida_df.reset_index(drop=True, inplace=True)

    return salida_df

if __name__ == "__main__":
    # Construir el DataFrame con textos
    dataframe_brown = pd.DataFrame({
        'text': construye_textos() + construye_textos() + construye_textos() + construye_textos()
    })
    
    print("DataFrame original:")
    print(dataframe_brown)
    
    # Ejecución secuencial
    tiempo_inicial_secuencial = time.time()
    df_procesado_secuencial = procesar_df(dataframe_brown)
    tiempo_total_secuencial = time.time() - tiempo_inicial_secuencial
    
    print("\nEjecución secuencial:")
    print(df_procesado_secuencial.head())
    print("Tiempo total secuencial:", tiempo_total_secuencial, "segundos")

    # Ejecución paralela
    trozos_df = np.array_split(dataframe_brown, 12)

    tiempo_inicial_paralelo = time.time()
    pool = Pool(processes=4)  # Decide el número de procesos a crear
    resultados = pool.map(procesar_df, trozos_df)
    pool.close()
    pool.join()
    df_procesado_paralelo = pd.concat(resultados)
    tiempo_total_paralelo = time.time() - tiempo_inicial_paralelo

    print("\nEjecución paralela:")
    print(df_procesado_paralelo.head())
    print("Tiempo total paralelo:", tiempo_total_paralelo, "segundos")