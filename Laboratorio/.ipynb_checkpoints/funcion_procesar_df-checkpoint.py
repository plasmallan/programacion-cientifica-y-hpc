import pandas as pd
import multiprocessing as mp
import numpy as np
import time
#modulo para usar expresiones regulares
import re
from multiprocessing import Pool
#importación del corpus brown
import nltk
nltk.download('brown')
from nltk.corpus import brown

# Función que construye el DataFrame para su procesamiento, con una frase por fila
def construye_textos():
    return [" ".join(np.random.permutation(sents)) for sents in brown.sents()]

# Función que reemplaza comillas dobles
def reemplazar_comillas(texto):
    return texto.apply(lambda text: text.replace("``", '"'))

# Función que convierte todas las palabras a minúsculas
def a_minusculas(texto):
    return texto.apply(lambda text: text.lower())

# Función que cuenta palabras de cada fila del dataframe
def contar_palabras(texto):
    return texto.apply(lambda text: len(re.split(r"(?:\s+)|(?:,)|(?:\-)", text)))
    
# Función que se aplica
def procesar_df(df):
    # Se hace copia del dataframe para no modificarlo
    salida_df = df.copy()

    # Reemplaza las comillas
    salida_df['text'] = reemplazar_comillas(salida_df['text'])

    # Pasa el texto a minúsculas
    salida_df['text'] = a_minusculas(salida_df['text'])

    # Cuenta el número de palabras y construye columna nueva con nombre num_palabras
    salida_df['num_palabras'] = contar_palabras(salida_df['text'])

    # Elimina los textos demasiado largos
    texto_largo_para_eliminar = salida_df[salida_df['num_palabras'] > 50]
    salida_df.drop(texto_largo_para_eliminar.index, inplace=True)

    # Elimina los textos demasiado cortos
    texto_corto_para_eliminar = salida_df[salida_df['num_palabras'] < 10]
    salida_df.drop(texto_corto_para_eliminar.index, inplace=True)

    # Reinicializa los índices
    salida_df.reset_index(drop=True, inplace=True)

    return salida_df