import threading
import time
import random

class Barberia:
    def __init__(self):
        # Mecanismos y variables de control clientes y acceso a la sala
        self.entrarEnSala = threading.Semaphore(1)  # Controla la entrada en la sala de espera
        self.clientesEnSala = threading.Semaphore(3)  # Controla el número máximo de clientes en la sala
        self.exclusionMutuaSala = threading.Lock()  # Garantiza exclusión mutua para modificaciones de variables de control de sala o acceso a secciones

        # Mecanismos y variables de control del barbero
        self.sillon = threading.Semaphore(1)  # Controla el acceso al sillón de corte
        self.barberoOcupado = threading.Semaphore(0)  # Indica si el barbero está ocupado
        self.dormir = threading.Semaphore(0)  # Permite que el barbero duerma hasta ser despertado

    def actividadBarbero(self):
        while True:
            # Protocolo inicial
            with self.exclusionMutuaSala:
                if self.clientesEnSala._value == 0:
                    print("El barbero se va a dormir")
                    self.dormir.acquire()
            
            print("El barbero da paso a un cliente\n")
            self.barberoOcupado.release()  # El barbero está disponible para cortar pelo
            
            print("El barbero corta a un cliente\n")
            time.sleep(5)
            self.sillon.release()  # El cliente ha sido atendido y deja el sillón
            
            # Protocolo final
            with self.exclusionMutuaSala:
                if self.clientesEnSala._value == 0:
                    print("El barbero se va a dormir")
                    self.dormir.acquire()

    def actividadCliente(self):
        print("El cliente "+threading.currentThread().name+" llega a la puerta de la barbería\n")
        
        # Protocolo inicial
        self.entrarEnSala.acquire()
        with self.exclusionMutuaSala:
            if self.clientesEnSala._value > 0:
                print("El cliente "+threading.currentThread().name+" entra en la sala de espera\n")
                self.clientesEnSala.acquire()
                self.entrarEnSala.release()
            else:
                print("El cliente "+threading.currentThread().name+" no puede entrar, la sala está llena\n")
                self.entrarEnSala.release()
                return  # El cliente se va sin esperar
        
        print("El cliente "+threading.currentThread().name+" está en el sillón\n")
        self.sillon.acquire()  # El cliente está en el sillón de corte
        time.sleep(3)
        print("El cliente "+threading.currentThread().name+" abandona la barbería\n")
        self.clientesEnSala.release()  # El cliente ha sido atendido y deja la sala de espera

if __name__=="__main__":
    barberia = Barberia()
    barbero_thread = threading.Thread(target=barberia.actividadBarbero)
    barbero_thread.start()

    for i in range(6):
        cliente_thread = threading.Thread(target=barberia.actividadCliente)
        cliente_thread.start()